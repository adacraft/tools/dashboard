module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/tools/' + process.env.CI_PROJECT_NAME + '/'
      : '/'
  }